//
//  ViewController.m
//  d
//
//  Created by Apple on 6/12/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize serviceType = _serviceType, session = _session, advertiser = _advertiser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.serviceType = [NSString stringWithFormat:@"snd-srv"];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated {
    NSLog(@"viewDidAppear");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) setServiceType {
    NSLog(@"Service Set");
    speakerOrHost = serverSwitch.selectedSegmentIndex;
    
    serverSwitch.hidden = YES;
    
    if (speakerOrHost == 0) {
        NSLog(@"Host Mode Enabled");
        ServerOrClientLabel.hidden = NO;
        selectMusicButton.hidden = NO;
        findSlaveDevicesButton.hidden = NO;
        ServerOrClientLabel.text = @"DJ Host Mode";

        //This is the leader who sets up the song to play and stuff
                
//        MCNearbyServiceAdvertiser *advertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:myPeerID discoveryInfo:nil serviceType:self.serviceType];
//        advertiser.delegate = self;
//        [advertiser startAdvertisingPeer];
        
    }
    if (speakerOrHost == 1) {
        NSLog(@"Listening Mode Enabled");
        ServerOrClientLabel.text = @"Listening Mode";
        //This is the slave device that recieves audio from the leader
        //In this case, it will set up an advertiser, not a browser
        
        setNameLabel.hidden = NO;
        nameField.hidden = NO;
        set2.hidden = NO;
    }
}

- (IBAction) startAdvertising {
    if (!([nameField.text length] < 1)) {
        [nameField resignFirstResponder];
        NSLog(@"Name: %@", nameField.text);
        NSLog(@"Start Advertising Method Called");
        ServerOrClientLabel.hidden = NO;
        MCPeerID *myPeerID = [[MCPeerID alloc] initWithDisplayName:nameField.text];
        self.advertiser = [[MCNearbyServiceAdvertiser alloc]
                                                        initWithPeer:myPeerID
                                                       discoveryInfo:nil
                                                         serviceType:self.serviceType];
        self.advertiser.delegate = self;
        [self.advertiser startAdvertisingPeer];
        
        setNameLabel.hidden = YES;
        nameField.hidden = YES;
        set2.hidden = YES;

    } else {
        NSLog(@"Name Field may be blank...");
        UIAlertView *namingError = [[UIAlertView alloc] initWithTitle:@"Name Error" message:@"You left the name blank. FIX IT NOW!" delegate:self cancelButtonTitle:@"Fine..." otherButtonTitles:nil, nil];
        [namingError show];
    }
}

-(void) loadController2 {
    //This will set up a browser to find advertisers to send data to
    MCPeerID *myPeerID = [[MCPeerID alloc] initWithDisplayName:@"DJ"];
    self.session = [[MCSession alloc] initWithPeer:myPeerID
                                        securityIdentity:nil
                                    encryptionPreference:MCEncryptionNone];
    self.session.delegate = self;
    MCNearbyServiceBrowser *browser = [[MCNearbyServiceBrowser alloc]
                                       initWithPeer:myPeerID
                                       serviceType:self.serviceType]; //@"surr-audio"];
    browserVC = [[MCPeerPickerViewController alloc] initWithBrowser:browser session:self.session];
    browserVC.delegate = self;

    [self presentViewController:browserVC animated:YES completion:nil];
}

- (void) peerPickerViewControllerWasCancelled:(MCPeerPickerViewController *)picker {
    NSLog(@"Cancelled");
    [browserVC dismissViewControllerAnimated:YES completion:NULL];
}

- (void)peerPickerViewController:(MCPeerPickerViewController *)picker didConnectPeers:(NSArray *)peerIDs {
    
}

- (BOOL)peerPickerViewController:(MCPeerPickerViewController *)picker shouldPresentNearbyPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info
{
    return YES;
}
/*
- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData*)context invitationHandler:(void(^)(BOOL accept, MCSession *session))invitationHandler {
    
}
 */

- (IBAction) setMusic {
    
}

#pragma mark MCSessionDelegate
// Remote peer changed state
- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
}

// Received data from remote peer


// Received resource from remote peer as a file written into a temporary location - the app is responsible for moving the file to a permanent location within its sandbox
- (void)session:(MCSession *)session didReceiveResourceAtURL:(NSURL *)resourceURL fromPeer:(MCPeerID *)peerID {
    NSLog(@"Session did receive resource at url");
    
}

// Received a byte stream from remote peer
- (void)session:(MCSession *)session didReceiveStream:(NSInputStream*)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    NSLog(@"Session did receive stream");
    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    //self.invitationHandler(YES, self.session);
        
    
    if (buttonIndex == 0) {
        
        NSLog(@"Connected Canceled");
        //SUCKS!
        
    }
    else {
       
        NSLog(@"Connected Accepted");
        

    }
    
}


#pragma mark MCNearbyServiceAdvertiserDelegate

- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser
didReceiveInvitationFromPeer:(MCPeerID *)peerID
       withContext:(NSData*)context invitationHandler:(void(^)(BOOL accept,
                                                               MCSession *session))invitationHandler {
    
    NSLog(@"Session did receive invitation from peer (%@)", peerID);
    
    self.invitationHandler = invitationHandler;
    
    [session nearbyConnectionDataForPeer:peerID withCompletionHandler:^(NSData *connectionData, NSError *error) {
        
        NSLog(@"Got the Connection Data");
        [session connectPeer:peerID withNearbyConnectionData:connectionData timeout:3];
        NSLog(@"A connection should have occured");
        if (!error) {
            NSLog(@"No error, trying to connect...");
            
        }
        else {
            NSLog(@"There was some sort of error when trying to get the peer connection data");
        }
    }];


    
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data
       fromPeer:(MCPeerID *)peerID {
    
    NSLog(@"Session did receive data");
    
    NSString *text = [[NSString alloc] initWithData:data
                                           encoding:NSASCIIStringEncoding];
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:peerID.displayName
                       
                                                 message:text
                       
                                                delegate:nil
                       
                                       cancelButtonTitle:nil
                       
                                       otherButtonTitles:@"OK", nil];
    
    [av show];
    
}



@end


