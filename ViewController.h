//
//  ViewController.h
//  d
//
//  Created by Apple on 6/12/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface ViewController : UIViewController <MCSessionDelegate, MCPeerPickerViewControllerDelegate, UITextFieldDelegate,MCNearbyServiceAdvertiserDelegate> {
    IBOutlet UIButton *set2;
    IBOutlet UITextField *nameField;
    IBOutlet UISegmentedControl *serverSwitch;
    IBOutlet UIButton *selectMusicButton;
    IBOutlet UIButton *findSlaveDevicesButton;
    IBOutlet UILabel *setNameLabel;
    IBOutlet UILabel *SatelliteStreamLabel;
    IBOutlet UILabel *ServerOrClientLabel;
    MCSession *session;
    MCPeerPickerViewController *browserVC;
    int speakerOrHost;
}

@property NSString *serviceType;
@property MCSession *session;
@property MCNearbyServiceAdvertiser *advertiser;

-(IBAction) loadController2;
-(IBAction) setServiceType;
-(IBAction) startAdvertising;
-(IBAction) setMusic;
- (BOOL)peerPickerViewController:(MCPeerPickerViewController *)picker shouldPresentNearbyPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info;

typedef void(^HandlerBlock)(BOOL accept, MCSession *session);

@property (copy) HandlerBlock invitationHandler;

@end
